# Bitbucket Pipelines image AWS CLI + ZIP

This is basically the same as [Bitbucket’s official Pipelines image for AWS CLI](https://hub.docker.com/r/atlassian/pipelines-awscli/), just with the added ZIP command, because AWS sometimes deals with ZIP files.
